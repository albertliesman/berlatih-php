@extends('templates.layout')


@section('content')
    <div class="container">
       
        <div class="row">
        <table class="table">
  <thead>
    <tr>
      <th scope="col">No</th>
      <th scope="col">Nama</th>
      <th scope="col">Umur</th>
      <th scope="col">Bio</th>
    </tr>
  </thead>
  <tbody>
      @foreach($Casts as $cast)
    <tr>
      <th scope="row">1</th>
      <td>{{$cast->nama}}</td>
      <td>{{$cast->umur}}</td>
      <td>{{$cast->bio}}</td>
    </tr>
    @endforeach
  </tbody>
</table>
        </div>
    </div>
@endsection