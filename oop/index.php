<?php 

require('frog.php');
require_once('ape.php');

// Release 0

$sheep = new animal("Shaun");

echo "Name: " . $sheep-> name . "<br>";
echo "legs: " . $sheep-> legs . "<br>";
echo "Cold blooded: " . $sheep-> cold_blooded . "<br>" . "<br>";

// Release 1
$kodok = new frog("Buduk");

echo "Name: " . $kodok-> name . "<br>";
echo "legs: " . $kodok-> legs . "<br>";
echo "Cold blooded: " . $kodok-> cold_blooded . "<br>";
echo "Jump: ";
$kodok -> jump();
echo "<br>";

$sungokong = new ape("Kera Sakti");

echo "Name: " . $sungokong-> name . "<br>";
echo "legs: " . $sungokong-> legs . "<br>";
echo "Cold blooded: " . $sungokong-> cold_blooded . "<br>";
echo "Yell: ";
$sungokong -> yell();
echo "<br>";

?>