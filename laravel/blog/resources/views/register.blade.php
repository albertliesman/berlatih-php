<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form Page</title>
</head>
<body>

    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>

    <form action="/welcome" method="POST">
        @csrf
        <label for="firstname">First name:</label> <br> 
        <input type="text" id="firstname" name="fname"> <br> <br>

        <label for="lastname">Last name:</label> <br> 
        <input type="text" id="lastname" name="lname"> <br> <br>

        <label for="">Gender:</label> <br> 
        <input type="radio" id="gender" name="gander"> Male <br>
        <input type="radio" id="gender" name="gander"> Female <br>
        <input type="radio" id="gender" name="gander"> Other <br> <br>

        <label for="nationality">Nationality:</label> <br> <br>
        <select name="" id="nationality">
            <option value="Indonesian">Indonesian</option>
            <option value="Singaporean">Singaporean</option>
            <option value="Malaysian">Malaysian</option>
        </select> 
        <br> <br>

        <label for="">Language Spoken:</label> <br>
        <input type="checkbox" id="bahasa"> <label for="bahasa">Bahasa Indonesia</label> <br>
        <input type="checkbox" id="english"> <label for="english">English</label> <br>
        <input type="checkbox" id="other"> <label for="other">Other</label> <br> <br>

        <label for="bio">Bio: </label> <br>
        <textarea name="bio" id="bio" cols="30" rows="10"></textarea> <br>

        <button type="submit">Sign Up</button>


    </form>


    
</body>
</html>